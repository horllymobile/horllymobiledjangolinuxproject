from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import GoalStatus, ScrumyGoals, User
from .forms import CreateGoalForm, SignupForm, MoveGoalForm
from django.contrib.auth.models import Group
from django.contrib.admin.views.decorators import staff_member_required
from random import randint


def index(request):
    group = Group.objects.get(name="Developer")
    form = SignupForm()
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            group.user_set.add(form.save())
            return HttpResponse("Your account has been created successfully")
    context = {
        'form': form,
    }
    return render(request, 'horlamidex1scrumy/index.html', context)


def move_goal(request, goal_id):
    my_id = goal_id
    move_goal_form = MoveGoalForm()
    goal = ScrumyGoals.objects.get(goal_id=my_id)

    # Group developer
    developer_group = Group.objects.get(name="Developer")
    developer_user = developer_group.user_set.all()

    # Group quality
    quality_group = Group.objects.get(name="Quality Assurance")
    quality_user = quality_group.user_set.all()

    # Group admin
    admin_group = Group.objects.get(name="Admin")
    admin_user = admin_group.user_set.all()

    # Group owner
    owner_group = Group.objects.get(name="Owner")
    owner_user = owner_group.user_set.all()

    # Statuses
    weekly_status = GoalStatus.objects.get(status_name = "Weekly Goal")
    daily_status = GoalStatus.objects.get(status_name = "Daily Goal")
    verify_status = GoalStatus.objects.get(status_name__contains = "Verify")
    done_status = GoalStatus.objects.get(status_name__contains= "Done")
    
    # print(quality_group.name)
    current_user = request.user
    print(current_user)
    if request.method == 'POST':
        move_goal_form = MoveGoalForm(request.POST)
        if move_goal_form.is_valid():
            if move_goal_form.cleaned_data["goal_status"] is not None:
                if current_user in developer_user and goal.user == current_user:
                    if move_goal_form.cleaned_data["goal_status"] != done_status:
                        goal.goal_status = move_goal_form.cleaned_data["goal_status"]
                        goal.save()
                        return HttpResponseRedirect('/horlamidex1scrumy/home')
                elif current_user in quality_user:
                    if goal.user == current_user:
                        if move_goal_form.cleaned_data['goal_status'] != weekly_status:
                            goal.goal_status = move_goal_form.cleaned_data["goal_status"]
                            goal.save()
                            return HttpResponseRedirect('/horlamidex1scrumy/home')
                        else:
                            return HttpResponseRedirect('/horlamidex1scrumy/home')
                        
                    elif current_user != goal.user:
                        print(current_user)
                        if current_user in quality_user:
                            if goal.goal_status == verify_status:
                                if  move_goal_form.cleaned_data['goal_status'] == done_status:
                                    goal.goal_status = move_goal_form.cleaned_data["goal_status"]
                                    goal.save()
                                    return HttpResponseRedirect('/horlamidex1scrumy/home')
                elif current_user in admin_user:
                    if goal.user == current_user or goal.user != current_user:
                        goal.goal_status = move_goal_form.cleaned_data["goal_status"]
                        goal.save()
                        return HttpResponseRedirect('/horlamidex1scrumy/home')
                elif current_user in owner_user:
                    if goal.user == current_user:
                        goal.goal_status = move_goal_form.cleaned_data["goal_status"]
                        goal.save()
                        return HttpResponseRedirect('/horlamidex1scrumy/home')
                else:
                    return HttpResponseRedirect('/horlamidex1scrumy/home')
                
    def get_current_user_group():
        group = None
        if request.user in developer_user:
            group = developer_group.name
        elif request.user in admin_user:
            group = admin_group.name
        elif request.user in quality_user:
            group = quality_group.name
        elif request.user in owner_user:
            group = owner_group.name
        return group
    context = {
        'form': move_goal_form,
        'goal': goal,
        'current_user': current_user,
        'group':get_current_user_group
    }
    return render(request, 'horlamidex1scrumy/move_goal.html', context)


def add_goal(request):
    scrumy_goal_form = CreateGoalForm()

    # Assigning the request user to current user variable
    current_user = request.user

    #Instanciating all Groups
    developer_group = Group.objects.get(name="Developer")
    quality_group = Group.objects.get(name="Quality Assurance")
    admin_group = Group.objects.get(name="Admin")
    owner_group = Group.objects.get(name="Owner")
    
    # Groups by user
    developer_user = developer_group.user_set.all()
    quality_user = quality_group.user_set.all()
    admin_user = admin_group.user_set.all()
    owner_user = owner_group.user_set.all()


    if request.method == 'POST':
        # Getting random number
        my_id = randint(1000, 9999)

        scrumy_goal_form = CreateGoalForm(request.POST)
        if scrumy_goal_form.is_valid(): # Checking form for validity
            if scrumy_goal_form.cleaned_data['goal_name'] is not None: # Finding out if scrumy_goal_form.cleaned_data['goal_name'] not empty
                if current_user in developer_user or current_user in quality_user or current_user in owner_user or current_user in admin_user: # Checking if the user is part of the groups
                    weekly_status = GoalStatus.objects.get(status_name__contains= "Weekly") # Instanciating weekly goal

                    try:
                        while ScrumyGoals.objects.get(goal_id= my_id) is not None: # checking if the id is not alread exist with a scrumygoal
                            my_id = randint(1000, 9999) # getting another id if former on exits
                    except:
                        # Creating new goal
                        new_goal = ScrumyGoals.objects.create(goal_status = weekly_status,
                         user = current_user, # Assigning the goal user go current_user
                         goal_name = scrumy_goal_form.cleaned_data['goal_name'], # Assigning the goal name to the form name data
                         goal_id = my_id, # Assigning the goal id to the random id
                         created_by = current_user.first_name, # Assigning created by to the current user first name
                         moved_by = current_user.first_name, # vice verser
                         owner = current_user.first_name # vice verser
                         )
                        new_goal.save() # Saving the new goal
                        scrumy_goal_form = CreateGoalForm() #clearing the field by re-instanciating the form
                else:
                    scrumy_goal_form = CreateGoalForm()
            else:
                scrumy_goal_form = CreateGoalForm()
            
    else:
        scrumy_goal_form = CreateGoalForm()
    

    # Function for getting user group
    def get_current_user_group():
        group = None
        if request.user in developer_user:
            group = developer_group.name
        elif request.user in admin_user:
            group = admin_group.name
        elif request.user in quality_user:
            group = quality_group.name
        elif request.user in owner_user:
            group = owner_group.name
        return group

    # all data
    context = {
        'form': scrumy_goal_form,
        'current_user': request.user,
        'group': get_current_user_group
    }
    return render(request, 'horlamidex1scrumy/add_goal.html', context)


def home(request):
    # All existing user
    user = User.objects.all()

    # Goal Statuses
    weekly_status = GoalStatus.objects.get(status_name="Weekly Goal")
    daily_status = GoalStatus.objects.get(status_name="Daily Goal")
    verify_status = GoalStatus.objects.get(status_name="Verify Goal")
    done_status = GoalStatus.objects.get(status_name="Done Goal")

    # Group developer
    developer_group = Group.objects.get(name="Developer")
    developer_user = developer_group.user_set.all()

    # Group quality
    quality_group = Group.objects.get(name="Quality Assurance")
    quality_user = quality_group.user_set.all()

    # Group admin
    admin_group = Group.objects.get(name="Admin")
    admin_user = admin_group.user_set.all()


    # Group owner
    owner_group = Group.objects.get(name="Owner")
    owner_user = owner_group.user_set.all()


    try:
        weekly_goals = weekly_status.scrumygoals_set.all()
        daily_goals = daily_status.scrumygoals_set.all()
        verify_goals = verify_status.scrumygoals_set.all()
        done_goals = done_status.scrumygoals_set.all()

    except ScrumyGoals.DoesNotExist:
        return HttpResponse()
    
    # Function for getting current user group
    def get_current_user_group():
        group = None
        if request.user in developer_user:
            group = developer_group.name
        elif request.user in admin_user:
            group = admin_group.name
        elif request.user in quality_user:
            group = quality_group.name
        elif request.user in owner_user:
            group = owner_group.name
        return group
    
    # All context
    context = {
        'users': user,
        'weekly_goals': weekly_goals,
        'daily_goals': daily_goals,
        'verify_goals': verify_goals,
        'done_goals': done_goals,
        'scrumygoals': ScrumyGoals,
        'current_user': request.user,
        'group': get_current_user_group()
    }
    return render(request, 'horlamidex1scrumy/home.html', context)

