from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
from .models import ChatMessage, Connection
import boto3



# Create your views here.
@csrf_exempt
def test(request):
    return JsonResponse({'message':'hello Daud'}, status=200)

def _parse_body(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)

@csrf_exempt
def connect(request):
    connect_id = Connection()
    body = _parse_body(request.body)
    connect_id.connection_id = body['connectionId']
    connect_id.save()
    return JsonResponse({'message':'connect successfully'}, status=200)

@csrf_exempt
def disconnect(request):
    body = _parse_body(request.body)
    connection_id = Connection.objects.get(connection_id = body['connectionId']).delete()
    return JsonResponse({'message':'disconnect successfully'}, status=200)


def _send_to_connection(connection_id, data):
    gatewayapi = boto3.client(
        'apigatewaymanagementapi',
        endpoint_url='https://6ym9ydu092.execute-api.af-south-1.amazonaws.com/test/',
        region_name="af-south-1",  aws_access_key_id='AKIAJWDSG3FT3OHGE7SA',
        aws_secret_access_key='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'
        )
    return gatewayapi.post_to_connection(ConnectionId = connection_id, Data = json.dumps(data).encode('utf-8'))

@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)
    username = body['body']['username']
    content = body['body']['content']
    timestamp = body['body']['timestamp']
    chat = ChatMessage.objects.create(username = username, message = content ,timestamp = timestamp)
    chat.save()
    connections = Connection.objects.all()
    data = {'messages':[body]}
    for connection in connections:
        _send_to_connection(connection.connection_id, data)
    return HttpResponse("")

@csrf_exempt
def get_recent_messages(request):
    body = _parse_body(request.body)
    chat_message = ChatMessage.objects.all().order_by('-timestamp')
    connections = Connection.objects.all()
    messages = {
        "messages":[]
    }
    for object in chat_message:
        messages["messages"].append({"username": object.username, "content": object.message, "timestamp": object.timestamp })
    data = messages
    for connection in connections:
        _send_to_connection(connection_id=connection.connection_id, data=data)
    return JsonResponse({"message":data}, status=200)
