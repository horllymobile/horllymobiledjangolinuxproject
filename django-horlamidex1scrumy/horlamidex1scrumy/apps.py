from django.apps import AppConfig


class Horlamidex1ScrumyConfig(AppConfig):
    name = 'horlamidex1scrumy'
