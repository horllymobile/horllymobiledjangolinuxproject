from django.http import HttpResponse
from django.shortcuts import render
from .models import GoalStatus, ScrumyGoals, User
from random import randint


def index(request):
    goal_name = ScrumyGoals.objects.filter(goal_name__contains="Learn Django")
    return HttpResponse(goal_name)


def move_goal(request, goal_id):
    myId = goal_id
    try:
        obj = ScrumyGoals.objects.get(goal_id=myId)
    except ScrumyGoals.DoesNotExist:
        return render(request, "horlamidex1scrumy/exception.html",
                      {'error': 'A record with that goal id does not exist'})
    return HttpResponse(obj.goal_name)


def add_goal(request):
    generated_id = randint(1000, 9999)
    status = GoalStatus.objects.get(status_name="Weekly Goal")
    added_user = User.objects.get(username="Louis Oma")
    query = ScrumyGoals.objects.create(goal_status=status, user=added_user, goal_name="Keep Learning Django",
                                       goal_id=generated_id,
                                       created_by="louis", moved_by="louis", owner="louis")
    if ScrumyGoals.objects.get(goal_id=generated_id) is True:
        return HttpResponse("Reload")
    else:
        query.save()
        return HttpResponse("Added")


def home(request):
    user = User.objects.all()
    weekly_status = GoalStatus.objects.get(status_name="Weekly Goal")
    daily_status = GoalStatus.objects.get(status_name="Daily Goal")
    verify_status = GoalStatus.objects.get(status_name="Verify Goal")
    done_status = GoalStatus.objects.get(status_name="Done Goal")
    try:
        weekly_goals = weekly_status.scrumygoals_set.all()
        daily_goals = daily_status.scrumygoals_set.all()
        verify_goals = verify_status.scrumygoals_set.all()
        done_goals = done_status.scrumygoals_set.all()

    except ScrumyGoals.DoesNotExist:
        return HttpResponse()
    dictionary = {
        'users': user,
        'weekly_goals': weekly_goals,
        'daily_goals': daily_goals,
        'verify_goals': verify_goals,
        'done_goals': done_goals,
        'scrumygoals': ScrumyGoals
    }
    return render(request, 'horlamidex1scrumy/home.html', dictionary)

