==================================
			horlamidex1scrumy
==================================


horlamidex1scrumy is a simple, Django app to conduct 

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "horlamidex1scrumy" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'horlamidex1scrumy',
    ]

2. Include the horlamidex1scrumy URLconf in your project urls.py like this::

    path('horlamidex1scrumy/', include('horlamidex1scrumy.urls')),

3. Run ``python manage.py migrate`` to create the horlamidex1scrumy models.

4. Start the development server and visit http://127.0.0.1:8000/admin/ to create models
(you'll need the Admin app enabled).

5. Visit http://127.0.0.1:8000/polls/ to